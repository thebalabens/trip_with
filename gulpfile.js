'use strict';

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var watch  = require('gulp-watch');
var sass = require('gulp-sass');
var postcss      = require('gulp-postcss');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var csso = require('gulp-csso');
var browserSync = require('browser-sync').create();


var paths = {
    css:  ['public/css/**/*.css'],
    js:   ['public/js/script.js'],
    scss: ['public/scss/**/*.scss']
};

gulp.task('scss', function () {
    return gulp.src(paths.scss)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ autoprefixer({ browsers: ['ie >= 10', 'last 2 versions', '> 5%'] }) ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.stream());
});

gulp.task('js', function () {
    return gulp.src(paths.js)
        .pipe(concat('main.js'))
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'))
        .pipe(browserSync.stream());
});

gulp.task('css', function () {
    return gulp.src(paths.css)
        .pipe(csso({
            restructure: false,
            sourceMap: false,
            debug: true
        }))
        .pipe(gulp.dest('public/css'));
});

gulp.task('default', function() {
    browserSync.init({
        server: {
            baseDir: "./public"
        }
    });

    gulp.watch(paths.scss, ['scss']);
    gulp.watch(paths.js, ['js']);
    gulp.watch("./public/html/*.html").on('change', browserSync.reload);
});