(function () {
    'use strict';

    var $html = $('html');
    var $humburger = $('.btn_hamburger');
    var $window = $(window);
    var $tipsSlider = $('.tips_slider');
    var $datepicker = $('.datepicker');

    var $range = $('._range_price');
    var $range_end = $('.end');
    var result = $('.result_value');
    var percent = $('.percent_value');
    var quantity = $('.quantity_value');
    var price = $('.range_price');

    var $tabs = $('.guide__tabs');
    var $fullpage = $('#fullpage');

    // hum
    $humburger.click(function () {
        $(this).toggleClass('active');
        $('.header_nav').slideToggle();
        $('body, html').toggleClass('overflow-hidden');
    });

    // remove mob menu on resize
    $window.on('resize', function () {
        if ($('window').width() > 767) {
            $humburger.removeClass('active');
            $('body, html').removeClass('overflow-hidden');
        }
    });

    // mobile menu sub links
    $('.sub').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active').find('.sub_menu').slideToggle();
        $(this).siblings('.sub').removeClass('active').find('.sub_menu').slideUp();
    });

    // login
    $('.header').on('click', '._login', function (e) {
        e.preventDefault();
        $('.modal.login').show();
        $('.overlay').show().addClass('hidden-xs');
        $('section, footer').addClass('hidden-xs');
        $('.btn_hamburger').removeClass('active');
        $('.header_nav').hide();
        $('body, html').removeClass('overflow-hidden');
    });

    // settings user modal
    $('.edit').click(function () {
        $('.modal.settings').show();
        $('.overlay').show().addClass('hidden-xs');
        $('section').addClass('hidden-xs');
    });

    $('.add_link').click(function () {
        $(this).next().find('.btn').html('Добавлено').parent().parent().addClass('added');
    });

    $('.add_feedback .btn').click(function () {
        $(this).html('Добавлено').parent().parent().addClass('added');
    });

    $('._add_new_feedback').click(function () {
        $('._feedback_new_modal').show();
        $('.overlay').show().addClass('hidden-xs');
        $('section').addClass('hidden-xs');
    });

    $('._add_new_template').click(function () {
        $('.modal.templates').show();
        $('.overlay').show().addClass('hidden-xs');
        $('section').addClass('hidden-xs');
    });

    $('._feedback_edit').click(function () {
        $('._feedback_edit_modal').show();
        $('.overlay').show().addClass('hidden-xs');
        $('section').addClass('hidden-xs');
    });

    $('._feedback_remove').click(function () {
        $('._feedback_remove_modal').show();
        $('.overlay').show().addClass('hidden-xs');
        $('section').addClass('hidden-xs');
    });

    $('.modal .close').click(function () {
        $('.modal').hide();
        $('.overlay').hide().removeClass('hidden-xs');
        $('section').removeClass('hidden-xs');
    });

    $('._modal_back').click(function () {
        $(this).parents('.modal').hide();
        $('.overlay').hide().removeClass('hidden-xs');
        $('section').removeClass('hidden-xs');
    });


    // footer nav
    $('.footer_nav_title').click(function () {
        if (window.innerWidth <= 992) {
            $(this).next().slideToggle();
        }
    });

    // user online list toggle
    $('.users_title').click(function () {
        if ($('window').width() < 992) {
            $(this).toggleClass('active').next().toggle();
        }
    });

    // msg search block
    $('.search_block').click(function (e) {
        e.stopPropagation();

        if ($('.search_active').length === 0) {
            $('.search_outer').toggleClass('search_active').find('.search_input');
        } else {
            // submit form
        }

    });

    // msg editor
    if ($('#editor1').length > 0) {
        CKEDITOR.replace('editor1');
    }

    // close tips
    $('._close_tip').click(function () {
        $(this).parents('.tips_shadow').remove();
    });

    // tips slider
    if ($tipsSlider.length) {
        $tipsSlider.slick({
            fade: true,
            autoplay: true,
            autoplaySpeed: 4000,
            swipe: false,
            speed: 300,
            arrows: false,
            cssEase: 'linear'
        });
    }

    $('.next-tip').click(function (e) {
        e.preventDefault();
        $tipsSlider.slick('slickNext');
    });

    // datepciker
    if ($datepicker.length !== 0) {

        $.fn.datepicker.dates['en'] = {
            days: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            daysShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            today: "Сегодня",
            clear: "Clear",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 1
        };

        $datepicker.datepicker({
            format: 'dd MM yyyy',
            language: 'ru',
            weekStart: 1,
            autoclose: true
        });
    }

    // custom select
    $('select').each(function () {

        var $this = $(this),
            numberOfOptions = $(this).children('option').length;

        $this.addClass('hidden');

        $this.wrap('<div class="select"></div>');

        $this.after('<div class="styledSelect"></div>');

        var $styledSelect = $this.next('div.styledSelect');

        $styledSelect.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
            'class': 'options'
        }).insertAfter($styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }

        var $listItems = $list.children('li');

        $styledSelect.click(function (e) {
            $('div.styledSelect.active').each(function () {
                $(this).removeClass('active').next('ul.options').hide();
            });
            $(this).toggleClass('active').next('ul.options').toggle();
        });

        $listItems.click(function (e) {
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            $(this).parent().prev().prev().trigger('change');
        });

        $(document).click(function (e) {
            if (!$(e.target).is(".styledSelect, .options")) {
                $styledSelect.removeClass('active');
                $list.hide();
            }
        });

    });


    // paid method clicking
    $('.paid_block').click(function () {
        $(this).find('.paid_inner').addClass('active').parent().siblings().find('.active').removeClass('active');
    });

    // range
    if ($('.range').length > 0) {
        var $your_cash = $('.your_cash');
        var $client_cash = $('.client_cash');

        $('.range').rangeslider({
            polyfill: false,
            onSlide: function (position, value) {
                $your_cash.html(value * 2);
                $client_cash.html(value);
            }
        });
    }

    // custom scroll
    if ($('.scroll').length > 0) {
        $('.scroll').perfectScrollbar();
    }

    // landing range show
    $('._show_range').click(function (e) {
        e.stopPropagation();
        $(this).toggleClass('active').next().toggle();
    });

    // custom range
    if ($range.length > 0) {
        $range.rangeslider({
            polyfill: false
        }).on('input', function () {
            var a = this.value * 1000;
            var b = quantity.val();
            var c = percent.val() / 100;
            var d = 30;

            $range_end.html(this.value + ' ' + '000');
            result.html(a * b * d * c);
        });
    }

    percent.add(quantity, price).on('change', function () {
        var a = price.val() * 1000 || 20000;
        var b = quantity.val();
        var c = percent.val() / 100;
        var d = 30;
        result.html(a * b * d * c);
    });

    // close blocks on body click
    $(document).click(function (e) {
        $('.search_outer').removeClass('search_active');

        if (!$(e.target).is(".range_wrapper, .rangeslider, .range_values, .range_values span, .rangeslider__handle")) {
            $(".range_wrapper").hide();
            $("._show_range").removeClass('active');
        }
    });

    // choose logo in settings
    $('.logos_list img').click(function () {
        var id = $(this).attr('data-id');
        $(this).addClass('active').parent().siblings().find('img').removeClass('active');
    });

    // blog support form
    $('.support_btn').click(function () {
        $(this).next('.support_form').fadeToggle(250);
    });

    $('.support_form').submit(function (e) {
        e.preventDefault();
        $(this).hide().next().fadeIn();
    });

    $('._close').click(function () {
        $(this).parent().fadeOut();
    });

    // color picker for widget

    if ($('.color_picker').length > 0) {
        $('.color_picker').colorPicker({
            showHexField: false,
            onColorChange: function (id, value) {
                this.prev().prev().html(value);
            }
        });
    }

    // show / hide widget form on mobile

    $('.widget_link, .close_widget').click(function () {
        $('.widget_form_preview').toggle();
    });

    // widget form bg change

    $('input[type=radio][name=site_bg]').change(function () {
        var bg_color = this.value;
        var $form_preview = $(this).parents('.widget_form_preview');
        $form_preview.css('background-color', bg_color);
        if (bg_color === '#000') {
            $form_preview.addClass('dark');
        } else {
            $form_preview.removeClass('dark');
        }
    });

    // guide tabs

    $tabs.each(function () {
        var tab_blocks = $(this).find('.guide__tabs__block');
        var tab__nav__item;

        $(this).append("<div class='guide__tabs__nav'></div>");

        tab_blocks.each(function (i, item) {
            $(this).attr('data-tab', 'tab-' + i);
        });

        for (var i = 0; i < tab_blocks.length; i++) {
            $(this).find('.guide__tabs__nav')
                .append("<div class='guide__tabs__nav__item' data-tab='tab-" + i + "'></div>");
        }

        tab__nav__item = $(this).find('.guide__tabs__nav__item');

        tab__nav__item.eq(0).addClass('active');

        tab__nav__item.click(function () {
            var data = $(this).attr('data-tab');
            console.log(data);
            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().siblings("[data-tab=" + data + "]").fadeIn().addClass('active')
                .siblings('.guide__tabs__block').hide().removeClass('active');
        });

    });

    // guide mobile scroll

    $('.guide__step').click(function () {
        $(this).toggleClass('active').find('.guide__nav').toggle();
    });

    $('.guide__nav li a').click(function (e) {
        if ($(window).width() < 768) {
            e.preventDefault();
            var href = $(this).attr('href');
            $('html, body').animate({scrollTop: $(href).offset().top}, 1000);
        }
    });

    $('.anchor__list a').click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        $('html, body').animate({scrollTop: $(href).offset().top}, 1000);
    });

    // full page sections scroll

    $('.guide__nav-main').each(function () {
        $(this).find('a').each(function (i) {
            $(this).click(function (e) {
                e.preventDefault();
                $.fn.fullpage.moveTo(i + 2, 0);
                $.fn.fullpage.setAllowScrolling(true);
            });
        });
    });


    // tabs scroll

    if ($fullpage.length && $(window).width() > 767) {
        $fullpage.fullpage({
            dragAndMove: false,
            fadingEffect: false,
            afterLoad: function () {
                var loadedSection = $(this);
                var tabs = loadedSection.find('.guide__tabs');
                if (tabs.length > 0 && $(window).width() > 992) {
                    $.fn.fullpage.setAllowScrolling(false);
                    loadedSection.off('wheel');

                    loadedSection.on('wheel', _.throttle(function (evt) {
                        var delta = evt.originalEvent.deltaY;
                        var current = tabs.find('.guide__tabs__block.active');
                        var current__nav = tabs.find('.guide__tabs__nav__item.active');

                        if (delta > 0) {
                            var next = current.next('.guide__tabs__block');
                            var next__nav = current__nav.next('.guide__tabs__nav__item');

                            if (next.length > 0) {
                                current.hide().removeClass('active');
                                current__nav.removeClass('active');
                                next.fadeIn().addClass('active');
                                next__nav.addClass('active');
                            } else {
                                $.fn.fullpage.moveSectionDown();
                                $.fn.fullpage.setAllowScrolling(true);
                                loadedSection.off('wheel');
                            }
                        }
                        else {
                            var prev = current.prev('.guide__tabs__block');
                            var prev__nav = current__nav.prev('.guide__tabs__nav__item');

                            if (prev.length > 0) {
                                current.hide().removeClass('active');
                                current__nav.removeClass('active');
                                prev.fadeIn().addClass('active');
                                prev__nav.addClass('active');
                            } else {
                                $.fn.fullpage.moveSectionUp();
                                $.fn.fullpage.setAllowScrolling(true);
                                loadedSection.off('wheel');
                            }
                        }
                    }, 650, {'trailing': false}));
                }
            },
            onLeave: function (i, next) {
                var $nav = $('.guide__nav-main.fixed');

                if (next > 1 && next < 8) {
                    $('._nav').fadeIn(750);
                    $nav.find('a').eq(next - 2).addClass('active')
                        .parent().siblings().find('a').removeClass('active prev');
                    $nav.find('a').eq(next - 3).addClass('prev');
                } else {
                    $('._nav').fadeOut(100);
                }
            }
        });
    }

    // destroy full page scroll on mobile

    if ($fullpage.length) {
        $(window).resize(function () {
            if ($html.hasClass('fp-enabled') && $(window).width() < 768) {
                $.fn.fullpage.destroy('all');
            } else if (!$html.hasClass('fp-enabled') && $(window).width() > 767) {
                $fullpage.fullpage();
            }
        });
    }

    // pure tabs

    $('.tab_link').click(function (e) {
        e.preventDefault();
        var href = $(this).data('href');
        var block = $('.tab_block[data-id="' + href + '"]');
        if (!block.is(':visible')) {
            $('.tab_block').hide().removeClass('visible-xs');
            block.fadeIn();
        }
    });

    $('.hotel_tag .close').click(function () {
       $(this).parent().remove();
    });

    $(".header").load("./header.html");
    $(".footer").load("./footer.html");
})();